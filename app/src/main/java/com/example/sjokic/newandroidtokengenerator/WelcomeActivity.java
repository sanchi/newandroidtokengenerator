package com.example.sjokic.newandroidtokengenerator;

import android.app.Activity;
import android.content.Intent;
import android.net.Proxy;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class WelcomeActivity extends Activity {
    public static String WELCOME_EXTRA = "WELCOME_EXTRA";
    public static String NEW_ATTEMPT_EXTRA = "NEW_ATTEMPT_EXTRA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void goToProxy(View view){
        Intent proxyIntent = new Intent(this, ProxyActivity.class);

        EditText nameView = (EditText) findViewById(R.id.editText);
        String name = nameView.getText().toString();
        EditText surnameView = (EditText) findViewById(R.id.editText2);
        String surname = surnameView.getText().toString();
        String welcomeMessage = getString(R.string.welcome2,name,surname);

        proxyIntent.putExtra(WELCOME_EXTRA, welcomeMessage);
        proxyIntent.putExtra(NEW_ATTEMPT_EXTRA, true);
        startActivity(proxyIntent);
    }
}

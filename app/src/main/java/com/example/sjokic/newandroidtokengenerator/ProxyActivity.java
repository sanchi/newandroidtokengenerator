package com.example.sjokic.newandroidtokengenerator;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;

public class ProxyActivity extends Activity {
    public static final int AUTH_REQCODE = 1;
    private boolean authFailed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proxy);

        authFailed = !getIntent().getBooleanExtra(WelcomeActivity.NEW_ATTEMPT_EXTRA, true);
        TextView welcome2Text = (TextView)findViewById(R.id.textView3);
        String welcome2Message = getIntent().getStringExtra(WelcomeActivity.WELCOME_EXTRA);
        welcome2Text.setText(welcome2Message);
    }

    public void tryToAuthenticate(View view){
        if(!authFailed) {
            Intent authenticateIntent = new Intent(this, AuthenticatorActivity.class);
            startActivityForResult(authenticateIntent, AUTH_REQCODE);
        }else{
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == AUTH_REQCODE){
            if(resultCode == RESULT_OK){
                Intent tokensIntent = new Intent(this, TokensActivity.class);
                startActivity(tokensIntent);
            }else{
                TextView sorryText = (TextView)findViewById(R.id.textView4);
                sorryText.setText(getString(R.string.authfailed));
                authFailed = true;
            }

        }
    }
}

package com.example.sjokic.newandroidtokengenerator;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class AuthenticatorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authenticator);
    }

    public void onAuth(View view){
        switch(view.getId()){
            case R.id.button3:
                setResult(Activity.RESULT_OK);
                break;
            case R.id.button4:
                setResult(Activity.RESULT_CANCELED);
                break;
        }
        finish();
    }
}

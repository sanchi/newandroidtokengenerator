package com.example.sjokic.newandroidtokengenerator;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Random;

public class TokensActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tokens);

        String[] tokens = new String[20];
        Random r = new Random();
        for(int i=0; i<20; i++){
            Integer tokenValue = r.nextInt(1000000);
            tokens[i] = String.format("%06d", tokenValue);
        }

        ButtonAdapter tokenAdapter = new ButtonAdapter(this,android.R.layout.simple_list_item_1, tokens);

        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(tokenAdapter);
    }
}

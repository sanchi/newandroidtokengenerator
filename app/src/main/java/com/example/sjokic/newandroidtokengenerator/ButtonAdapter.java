package com.example.sjokic.newandroidtokengenerator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.Button;

/**
 * Created by sjokic on 7/13/16.
 */
public class ButtonAdapter extends ArrayAdapter<String> {
    private String[] items;
     public ButtonAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
         this.items = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = convertView;
        Button rowButtonHolder = null;

        if(view == null){
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_item_layout, null);
            rowButtonHolder = (Button)view.findViewById(R.id.button5);
            view.setTag(rowButtonHolder);
        }else{
            rowButtonHolder = (Button)view.getTag();
        }

        rowButtonHolder.setText(this.items[position]);

        return view;
    }
}

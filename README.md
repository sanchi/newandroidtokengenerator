# New Android Token Generator #

**Use-case:** 
Želimo mogućnost generisanja određenog broja token-a putem Android aplikacije. Dotični token-i bi se koristili od strane neke fiktivne aplikacije :) 
Takođe, generisanje tokena bi trebalo dozvoliti samo autentifikovanim korisnicima.

**User Interface:**
Na sledećoj slici je okvirni prikaz izgleda ekrana vaše aplikacije:
![screens.png](https://bitbucket.org/repo/RBgza7/images/650590461-screens.png)

Welcome: početni screen na kome korisnik ima mogućnost unosa svog imena i prezimena.
Proxy: screen koji obaveštava korisnika da bi trebalo da se autentifikuje. Takođe, isti screen se kasnije koristi za generisanje liste token-a.
Authenticator: screen sa pojednostavljenim načinom autentifikacije - od korisnika se očekuje da sâm potvrdi da li je autentifikovan ili ne.
Tokens: ovo je završni screen na kome se prikazuje lista tokena koja je izgenerisana od strane Proxy-a.

**Action Flow:**

Na sledećoj slici je prikaz tok akcija kroz koje bi korisnik trebao da prođe kako bi zadovoljio sve uslove za generisanje liste token-a:
![flow.png](https://bitbucket.org/repo/RBgza7/images/2107081076-flow.png)

1.  Korisnik je pokrenuo aplikaciju (Welcome screen)

2.  Korisnik bira opciju za prelazak na sledeći screen (Next dugme), nakon čega Proxy u header-u screen-a prikazuje poruku koja sadrži ime i prezime uneto na Welcome screen-u (npr. "Welcome, Petar Petrović!")

2.  Korisnik bira opciju za prelazak na sledeći screen, Authenticator.

3.  Na Authenticator screen-u, nakon odbaira bilo koje od ponuđenih opcija, treba preći natrag na Proxy screen, pri čemu bi, odabir "I AM" opcije trebao da signalizira Proxy screen-u da je sve OK, dok se u slučaju odabira "I AM NOT" opcije Proxy screen obaveštava o neuspešnoj autentifikaciji (CANCELLED). 

4.  Proxy screen je primio CANCELLED rezultat (neuspešna autentifikacija) --> tekst "Sorry, you'll need to authenticate before proceeding." se menja u "Authentication failed!" --> NEXT dugme preusmerava korisnika na Welcome screen.

5.  Proxy screen je primio OK rezultat (uspešna autentifikacija) --> tekst "Sorry, you'll need to authenticate before proceeding." se menja u "You've been authenticated." --> NEXT dugme generiše listu tokena sa kojom se korisnik preusmerava na Tokens screen koji i prikazuje generisanu listu tokena.


**Requirements:**

Aplikacija bi trebalo da zadovolji nekoliko zahteva:

* Izgled screen-ova bi trebao da bude približan izgledu screen-ova sa gornje slike, u smislu rasporeda komponenti (text view-ova, button-a, i dr.). 
Hint: ovde treba obratiti pažnju na položaj pojedinačnih komponenti, tj. uz koju ivicu matičnog screen-a su priljubljeni, da li se nalaze na centru screen-a i sl.
Nema zahteva po pitanju pojedinačnih komponenti - na vama je da odaberete željenu boju dugmića, pozadine, veličninu i stil prikazanih tekstova. 

* Tok rada aplikacije treba da prati dokumentovani tok ("Action Flow" sekcija).

* Broj tokena koje treba izgenerisati: 20. 

* Svaki token treba imati 6 cifara, pri čemu početne cifre mogu biti i nule (primer je dat na slici sa prikazom izgleda Tokens screen-a).

* Način generisanja tokena nije definisan - na vama je da odaberete način generisanja. U najprostijem slučaju možete generisati niz od 20 brojeva i kreirati string-ove dužine 6 karaktera (npr. "000001", "000002", ...). Cilj ove vežbe je da prođete kroz većinu UI feature-a, tako da akcenat nije na logici generisanja token-a, i ne bi trebalo mnogo da se opterećujete time.